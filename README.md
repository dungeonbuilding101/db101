**CMPUT 250 Project at the University of Alberta**

Winner of Gameplay Design award

---

## Team Cutecumber

1. Eisha Ahmed *Game Producer and Programmer* 
2. Matthew Fryer *Lead Designer and Writer*
3. Kieran Gilmore *Visual Artist*
4. Felipe Salas *Sound Artist*
5. Thomas Pham *Programmer*
6. Josh Smith *Programmer*

---

## To Play the Game

For an executable, email eisha@ualberta.ca
OR 
You will need to download the RPG Maker MV game engine and fork this repository


---

## Dungeon Building 101

You are a broke skeleton hired for a new job! 
Defend your master's dungeon against the "hero squad" by buying and setting traps.
May the "bad guys" win :) 