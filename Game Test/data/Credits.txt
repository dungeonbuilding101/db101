<block:-1,-3,10,10,offbot,center,hero>
CREDITS

\c[2]Producer
Eisha Ahmed

\c[2]Lead Designer
Matthew Fryer

\c[2]Artist
Kieran Gilmore

\c[2]Programmers
Eisha Ahmed
Thomas Pham
Josh Smith

\c[2]Sound
Felipe Salas

\c[2]Writer
Matthew Fryer

\c[2]Executive Producer
Dylan Nixon

\c[2]Plugins Used
Yanfly Engine:
PictureCommonEvents
ButtonCommonEvents
GabWindow
MapGoldWindow

u'ki:
OnMapBullet

GLAV:
RollCredits

Tyruswoo:
Camera Control

Terrax:
Lighting
</block>

<block:999,0,10,10,500,center,end>
THANKS FOR PLAYING!
</block>