﻿//=============================================================================
// μ'ki's On-Map Bullets v1.01
// MK_OnMapBullet.js
//=============================================================================

var Imported = Imported || {};
Imported.MK_OnMapBullet = true;
var Maki = Maki || {};
Maki.Bul = Maki.Bul || {};
Maki.Bul.Game_Map_setup = Game_Map.prototype.setup;
Game_Map.prototype.setup = function(mapId) {
    Maki.Bul.Game_Map_setup.call(this, mapId);
    this._bulletSource = 0;
    this._bulletHitting = 0;
    this._bulletHittingName = '';
	var exists = true;
	try {
		if ($bulletSprite) 
			exists = true;
	} catch (e) { $bulletSprite = [];
	for (var j = 0; j < Maki.Bul.MaxInstances; j++) {
        $bulletSprite[j] = new Bullet(j);
    }
	}
    for (var j = 0; j < Maki.Bul.MaxInstances; j++) {
        $bulletSprite[j] = new Bullet(j);
    }
};

//=============================================================================
/*:
 * @plugindesc v1.01 On-Map Bullets
 * @author μ'ki
 *
 * @param Max Instances
 * @desc Max bullet instances. This would slightly affect the game performance.
 * @default 30
 *
 * @param Player Hit Switch
 * @desc Switch ID triggered when the player character touches a bullet.
 * @default 51
 *
 * @param Hit Atk Variable
 * @desc Variable ID to store bullet's Atk value when hitting a target.
 * @default 52
 *
 * @param Trig Self Switch
 * @desc Target's self switch name triggered when touching a bullet.
 * @default D
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 * This plugin enables player character and events to spawn 'bullets' on the
 * game map, that technically are game pictures. The bullets disappear (erased)
 * when hitting obstacles (impassable things). If they hit an event (not the
 * spawning one), its self switch with name configured in the plugin parameter
 * will be turned on. If they hit the player character (not from itself),
 * the switch with id as configured in the plugin parameter will be turned on.
 * 
 * Some RPGs, and mainly action RPGs use such on-map bullet system. And thus
 * this is pretty useful to build an event-based action battle system.
 * 
 * This plugin is a remake from my previous work, RPG VX script 'BulletMod',
 * which is used in a game made by myself, The Mystery House.
 *  
 * Contact me for support/bug report:
 * listra92[at]gmail.com
 * https://www.facebook.com/don.listRA92
 *
 * ============================================================================
 * Plugin Command
 * ============================================================================
 * ShowBullet name source inst_id speed dir atk target fixed
 * 
 * where
 *  - name: picture file name to use (without extension)
 *  - source: character that spawns the bullet (-1: player, 0: this event,
 *    >0: designated event id)
 *  - inst_id: variable id to store instance index of the spawned bullet (begin
 *    from 0)
 *  - speed: the bullet speed (in tiles/sec)
 *  - dir: 1~8: down, left, right, up, SW, SE, NW, NE, 9: towards target,
 *    10: homing
 *  - speed: the bullet's Atk value
 *  - target: character to aim towards (for dir = 9 or 10)
 *  - fixed: whether not to rotate the bullet (can still be changed by
 *    MoveBullet command)
 *
 * MoveBullet inst_id x y angle
 * 
 * where
 *  - inst_id: bullet instance index to 'move'
 *  - x, y: destination position to move the bullet instance (in the map grid
 *    coordinate)
 *  - angle: rotation angle (CCW, from east)
 *
 * ============================================================================
 * Notes
 * ============================================================================
 *  - This plugin only provides rather 'raw' function to spawn bullets. You may
 *    need some special handlings with your own event commands/scripts if you
 *    want to extend some, e.g. to prevent player character spawn bullets while
 *    an autorun/triggered event is running since it is run in a parallel
 *    event.
 *
 * ============================================================================
 * Coming in the Next Version
 * ============================================================================
 *  - Prepare for use in μ'ki's Action System, a whole on-map action battle
 *    system to develop in future.
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * v1.01:
 *  - Fixed the bullets not applying Atk value when hitting the player character.
 * 
 */
//=============================================================================

//=============================================================================
// Parameter Variables
//=============================================================================

Maki.Bul.Parameters = PluginManager.parameters('MK_OnMapBullet');

Maki.Bul.MaxInstances = Number(Maki.Bul.Parameters['Max Instances']);
Maki.Bul.PlayerHit = Number(Maki.Bul.Parameters['Player Hit Switch']);
Maki.Bul.HitAtk = Number(Maki.Bul.Parameters['Hit Atk Variable']);
Maki.Bul.TriggerSS = String(Maki.Bul.Parameters['Trig Self Switch']);

Game_Character.prototype.collisionPoint = function(x, y) {
    if (this !== $gamePlayer && $gamePlayer.pos(x, y)) {
        return $gamePlayer;
    } else {
        var theChar = this;
        var events = $gameMap.eventsXyNt(x, y);
        var thevent = 0;
        events.forEach(function(event) {
            if (event !== theChar && event.isNormalPriority()) {
                thevent = event;
            }
        }, events);
        if (!thevent) {
            if ($gameMap.boat().posNt(x, y)) {
                return $gameMap.boat();
            }
            if ($gameMap.ship().posNt(x, y)) {
                return $gameMap.ship();
            }
        }
        return thevent;
    }
};

function Bullet() {
    this.initialize.apply(this, arguments);
}

Bullet.prototype = Object.create(Game_Picture.prototype);
Bullet.prototype.constructor = Bullet;

Bullet.prototype.initialize = function() {
    Game_Picture.prototype.initialize.call(this);
    this._bullet_dir = -1;
    this._speed = 0;
};

Bullet.prototype.show = function(name, source, speed, dir, target, scaleX, scaleY) {
    Game_Picture.prototype.show.call(this, name, 1,
        source.screenX(), source.screenY()-24, scaleX || 100, scaleY || 100,255,0);
    this._realX = source._realX;
    this._realY = source._realY;
    this._realX0 = this._realX;
    this._realY0 = this._realY;
    this._speed = speed;
    this._bullet_dir = dir;
    this._source = source;
    this._target = target;
    if (source === target) {
        this.erase();
    } else {
        if (dir === 9) {
            this._dx = speed/Math.sqrt((target._realX-source._realX)*
            (target._realX-source._realX)+(target._realY-source._realY)*(target._realY-
            source._realY))*(target._realX-source._realX);
            this._dy = speed/Math.sqrt((target._realX-source._realX)*
            (target._realX-source._realX)+(target._realY-source._realY)*(target._realY-
            source._realY))*(target._realY-source._realY);
        }
    }
};

Bullet.prototype.update = function() {
    if (this._name !== '') {
        var x_last = this._realX;
        var y_last = this._realY;
        switch (this._bullet_dir) {
        case 1:
            this._realY += this._speed;
            if (this._isRotate) this._angle = 270;
            break;
        case 2:
            this._realX -= this._speed;
            if (this._isRotate) this._angle = 180;
            break;
        case 3:
            this._realX += this._speed;
            if (this._isRotate) this._angle = 0;
            break;
        case 4:
            this._realY -= this._speed;
            if (this._isRotate) this._angle = 90;
            break;
        case 5:
            this._realX -= this._speed*0.707;
            this._realY += this._speed*0.707;
            if (this._isRotate) this._angle = 225;
            break;
        case 6:
            this._realX += this._speed*0.707;
            this._realY += this._speed*0.707;
            if (this._isRotate) this._angle = 315;
            break;
        case 7:
            this._realX -= this._speed*0.707;
            this._realY -= this._speed*0.707;
            if (this._isRotate) this._angle = 135;
            break;
        case 8:
            this._realX += this._speed*0.707;
            this._realY -= this._speed*0.707;
            if (this._isRotate) this._angle = 45;
            break;
        case 9:
            this._realX += this._dx;
            this._realY += this._dy;
            if (this._isRotate) this._angle = Math.atan2(-this._dy, this._dx) * 180 / Math.PI;
            break;
        case 10:
            var x2 = this._target._realX-this._source._realX;
            var y2 = this._target._realY-this._source._realY;
            this._dx = this._speed/Math.sqrt(x2*x2+y2*y2)*x2;
            this._dy = this._speed/Math.sqrt(x2*x2+y2*y2)*y2;
            this._realX += this._dx;
            this._realY += this._dy;
			if (this._isRotate) this._angle = Math.atan2(-this._dy, this._dx) * 180 / Math.PI;
            break;
        }
        var tw = $gameMap.tileWidth();
        this._x = Math.round((this._realX-$gameMap._displayX) * tw + tw / 2);
        var th = $gameMap.tileHeight();
        this._y = Math.round((this._realY-$gameMap._displayY) * th + th / 2);
        if (Math.round(x_last) !== Math.round(this._realX0) ||
            Math.round(y_last) !== Math.round(this._realY0)) {
            var x2 = Math.round(this._realX);
            var y2 = Math.round(this._realY);
            if (!$gameMap.checkPassage(x2, y2, 0x0f) && !$gameMap.isBoatPassable(x2, y2)) {
                this.erase();
            } else {
                var coli = this._source.collisionPoint(x2, y2);
                if (coli) {
                    if (coli === $gamePlayer) {
                        $gameSwitches.setValue(Maki.Bul.PlayerHit, true);
                        $gameMap._bulletSource = this._source;
                        $gameMap._bulletHitting = this;
                        $gameMap._bulletHittingName = this._name;
                        $gameVariables.setValue(Maki.Bul.HitAtk, this._atk);
                    } else if (coli !== $gameMap.boat() && coli !== $gameMap.ship()) {
                        var key = [$gameMap._mapId,coli.eventId(),Maki.Bul.TriggerSS];
                        $gameSelfSwitches.setValue(key, true);
                        $gameMap._bulletSource = this._source;
                        $gameMap._bulletHitting = this;
                        $gameMap._bulletHittingName = this._name;
                        $gameVariables.setValue(Maki.Bul.HitAtk, this._atk);
                    }
                    this.erase();
                }
            }
        }
        if (this._x<=0 || this._x>=Graphics._width ||
            this._y<=0 || this._y>=Graphics._height) {
            this.erase();
        }
    }
    Game_Picture.prototype.update.call(this);
};



Game_Map.prototype.createBullet = function(name, source, speed, dir, atk, target, isRotate) {
    var ret_id = 0;
	try {
		if ($bulletSprite) 
			exists = true;
	} catch (e) { $bulletSprite = [];
	for (var j = 0; j < Maki.Bul.MaxInstances; j++) {
        $bulletSprite[j] = new Bullet(j);
    }	
	}
    while (ret_id<Maki.Bul.MaxInstances &&
        $bulletSprite[ret_id]._name) ret_id++;
    if (ret_id<Maki.Bul.MaxInstances) {
        $bulletSprite[ret_id].show(name, source, speed/60, dir, target);
        $bulletSprite[ret_id]._atk = atk;
		$bulletSprite[ret_id]._isRotate = isRotate;
        return ret_id;
    }
};

Maki.Bul.Game_Screen_updatePictures = Game_Screen.prototype.updatePictures;
Game_Screen.prototype.updatePictures = function() {
    Maki.Bul.Game_Screen_updatePictures.call(this);
	try {
		if ($bulletSprite) 
			exists = true;
	} catch (e) { $bulletSprite = []; 
	for (var j = 0; j < Maki.Bul.MaxInstances; j++) {
        $bulletSprite[j] = new Bullet(j);
    } 
	}
    for (var j in $bulletSprite) {
        if ($bulletSprite[j]._name) {
            if ($gamePlayer._transferring) {
                $bulletSprite[j].erase();
            } else {
                $bulletSprite[j].update();
            }
        }
    }
};

Maki.Bul.Spriteset_Map_createPictures = Spriteset_Map.prototype.createPictures;
Spriteset_Map.prototype.createPictures = function() {
    Maki.Bul.Spriteset_Map_createPictures.call(this);
    for (var i = 0; i < Maki.Bul.MaxInstances; i++) {
        this._pictureContainer.addChild(new Sprite_Bullet(i));
    }
};

function Sprite_Bullet() {
    this.initialize.apply(this, arguments);
}

Sprite_Bullet.prototype = Object.create(Sprite_Picture.prototype);
Sprite_Bullet.prototype.constructor = Sprite_Bullet;

Sprite_Bullet.prototype.picture = function() {
	try {
		if ($bulletSprite) 
			exists = true;
	} catch (e) { $bulletSprite = []; 
	for (var j = 0; j < Maki.Bul.MaxInstances; j++) {
        $bulletSprite[j] = new Bullet(j);
    }
	}
    return $bulletSprite[this._pictureId];
};

Maki.Bul.Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
Game_Interpreter.prototype.pluginCommand = function(command, args) {
    Maki.Bul.Game_Interpreter_pluginCommand.call(this, command, args);
	try {
		if ($bulletSprite) 
			exists = true;
	} catch (e) { $bulletSprite = []; 
	for (var j = 0; j < Maki.Bul.MaxInstances; j++) {
        $bulletSprite[j] = new Bullet(j);
    } 
	}
    if (command === 'ShowBullet') {
        for (var j = 1; j < 8; j++) args[j] = eval(args[j] || '0');
        args[1] = this.character(args[1]);
        args[6] = this.character(args[6]);
        var ret_id = $gameMap.createBullet(args[0], args[1], args[3], args[4],
		    args[5], args[6], !args[7]);
		if (args[2]) $gameVariables[args[2]] = ret_id;
    }
	if (command === 'MoveBullet') {
        for (var j = 0; j < 3; j++) args[j] = eval(args[j] || '0');
		args[3] = eval(args[j] || '-1');
        if (args[0]) {
			$bulletSprite[args[0]]._realX = args[1];
			$bulletSprite[args[0]]._realY = args[2];
			if (0 <= args[3]) $bulletSprite[args[0]]._angle = args[3];
		}
    }
};